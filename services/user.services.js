; (function () {
	angular.module('app')
		.factory('UserService', ['$rootScope', '$fetch', '$window', 'CONSTANTS', function ($rootScope, $fetch, $window, CONSTANTS) {

			var srv = {
				login: function (username, password) {
					srv.logout();

					return $fetch.json(CONSTANTS.LOGIN_URL, {
						method: 'POST',
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						body: 'email=' + username + '&password=' + password
					})
						//.then(toJson)
						.then(function (data) {
							if (data && !data.error) {
								$window.localStorage.setItem('session', data.apiKey);
								$window.localStorage.setItem('user', JSON.stringify(data));
								//$localStorage.setItem('user', JSON.stringify(data));
							}
							return data;
						});
				},
				isLoggedIn: function () {
					return !!$window.localStorage.session;
				},
				logout: function () {
					$window.localStorage.removeItem('session');
					$window.localStorage.removeItem('user');
					//$localStorage.removeItem('user');
				},
				currentUser: function () {
					var val = $window.localStorage.getItem('user');
					return val ? JSON.parse(val) : null;
				},
				register: function (user) {
					var strbody = Object.keys(user)
						.map(function (key) {
							return key + '=' + user[key]
						})
						.join('&');
					//var strbody = JSON.stringify(user);

					return $fetch.json(CONSTANTS.REGISTER_URL, {
						method: 'POST',
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/x-www-form-urlencoded'
						},
						body: strbody
					})
						//.then(toJson)
						.then(function (data) {
							return data;
						});
				}
			};

			return srv;
		}]);
})();
