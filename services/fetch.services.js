; (function () {

	angular.module('app')
		.factory('$fetch', ['$window', '$q', '$state', 'CONSTANTS', function ($window, $q, $state, CONSTANTS) {
			// hot fix
			$window.Promise = $window.Promise ? $window.Promise : $q;
			var fetch = window.fetch || window.cordovaFetch;

			var srv = {
				json: function (url, ops) {
					return srv.raw(url, ops)
						.then(function (res) {
							var contentType = res.headers['Content-Type'] || (res.headers.get ? res.headers.get('Content-Type') : '');
							var isJson = contentType.indexOf('/json') > -1;
							if (isJson && res.json)
								return res.json();
							if (isJson && res.statusText)
								return JSON.parse(res.statusText);
							if (res.status == 400 || res.status == 404 || res.status == 400)
								return { error: true, message: 'Cannot load data' };
							return res;
						})
						.catch(function (error) {
							if (error === 'Network request failed' || error.message === 'Network request failed') {
								return Promise.reject({ error: true, message: 'Internet Not Available Please check' });
							}
							return Promise.reject(error);
						});
				}
			};
			return srv;
		}]);
})();
