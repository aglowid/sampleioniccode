; (function () {

	angular.module('app')
		.controller('LoginController', LoginController)


	function LoginController($scope, $rootScope, $state, UserService, $ionicLoading, $log, $ionicHistory) {
		this.$ionicLoading = $ionicLoading;
		this.$log = $log;
		this.$rootScope = $rootScope;
		$scope.submit = this.submit.bind(this, $scope, $state, UserService);
		//UserService.logout();
		/** reset **/
		$scope.username = $rootScope.username || '';
		$scope.password = $rootScope.password || '';
		$scope.hasServerError = false;
		$ionicHistory.clearHistory();
	}

	LoginController.prototype.submit = function ($scope, $state, UserService, valid, username, password) {
		if (!valid)
			return false;

		$scope.hasServerError = false;

		this.$ionicLoading.show({
			template: '<ion-spinner></ion-spinner>'
		});

		UserService.login(username, password)
			.then(function (res) {
				if (res && res.error) {
					$scope.hasServerError = res.message || true;
				} else if (res && !res.error) {
					this.$rootScope.name = res.name;
					this.$rootScope.email = res.email;
					this.$rootScope.password = '';
					$state.go('dashboard');
				}
			}.bind(this))
			.then(function () {
				this.$ionicLoading.hide();
			}.bind(this))
			.catch(function (err) {
				this.$log.error(err);
				this.$ionicLoading.show({
					template: err.message || JSON.stringify(err),
					duration: 1000 * 3
				});
			}.bind(this))


		return false;
	};

	LoginController.$inject = ['$scope', '$rootScope', '$state', 'UserService', '$ionicLoading', '$log', '$ionicHistory'];

})();
